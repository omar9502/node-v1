var map= L.map('main_map').setView([10.995090, -74.784130], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([10.995090, -74.784130]).addTo(map);
//L.marker([10.988417, -74.787778]).addTo(map);
//L.marker([10.990025, -74.788804]).addTo(map);

$.ajax({
      dataType: "json",
      url: "api/bicicletas",
      success: function(result){
            console.log(result);
            result.bicicleta.forEach(function(bici) {
          
                  L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
                  
            });
      }
}) 